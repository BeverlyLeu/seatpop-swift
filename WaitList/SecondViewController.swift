//
//  SecondViewController.swift
//  WaitList List
//
//  Created by BEVERLY LEU on 11/14/14.
//  Copyright (c) 2014 BEVERLY'S LAB. All rights reserved.
//

import UIKit

class SecondViewController: UIViewController, UITextFieldDelegate {
    
    
    @IBOutlet var txtWaitList: UITextField!
    @IBOutlet var txtName: UITextField!
    @IBOutlet var txtSize: UITextField!
    @IBOutlet var txtPhone: UITextField!
    @IBOutlet weak var activityIndicatorView: UIView! //BEVERLY
    let httpHelper = HTTPHelper()//BEVERLY
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    //Events
    @IBAction func btnAddWaitList_Click(sender: UIButton){
        waitlistMgr.addWaitList(txtName.text, size: txtSize.text, phone: txtPhone.text);
        
        //BEVERLY send party back to server
        
        let httpRequest = postPartyRequest(txtName.text, size: txtSize.text, phone: txtPhone.text)
        
        //BEVERLY
        
        self.view.endEditing(true)
        txtName.text = ""
        txtSize.text = ""
        txtPhone.text = ""
        
        //tblWaitLists.reloadData()
        waitlistMgr.reload()
        self.tabBarController?.selectedIndex = 0;
        
    }
    
    
    
    //BEVERLY
    
    
    func postPartyRequest(name: NSString, size: NSString, phone: NSString) -> NSMutableURLRequest {
        
        let httpRequest = httpHelper.buildRequest("add_party", method: "POST", authType: HTTPRequestAuthType.HTTPTokenAuth, requestContentType:HTTPRequestContentType.HTTPJsonContent )

        httpRequest.HTTPBody = "{\"name\":\"\(name)\",\"size\":\"\(size)\",\"phone\":\"\(phone)\"}".dataUsingEncoding(NSUTF8StringEncoding);
        
        httpHelper.sendRequest(httpRequest, completion: {(data:NSData!, error:NSError!) in
            // Display error
            if error != nil {
                let errorMessage = self.httpHelper.getErrorMessage(error)
                self.displayAlertMessage("Error", alertDescription: errorMessage)
                
                return
            }
            
        })

        
        return httpRequest
    }    //BEVERLY
    

    
    
    
    
    func displayAlertMessage(alertTitle:NSString, alertDescription:NSString) -> Void {//BEVERLY
        // hide activityIndicator view and display alert message
        self.activityIndicatorView.hidden = true
        let errorAlert = UIAlertView(title:alertTitle as String, message:alertDescription as String, delegate:nil, cancelButtonTitle:"OK")
        errorAlert.show()
    }
    
    //IOS Touch Functions
    override func touchesBegan(touches: Set<NSObject>, withEvent event: UIEvent) {
        self.view.endEditing(true)
    }
    //UITextFieldDelegate
    func textFieldShouldReturn(textField: UITextField) -> Bool // called when 'return' key pressed. return NO to ignore.
    {
        textField.resignFirstResponder();
        return true
    }
    
    
}

