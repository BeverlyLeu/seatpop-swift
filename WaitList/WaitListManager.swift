//
//  WaitListManager.swift
//  WaitList List
//
//  Created by BEVERLY LEU on 11/14/14.
//  Copyright (c) 2014 BEVERLY'S LAB. All rights reserved.
//

import UIKit

var waitlistMgr: WaitListManager = WaitListManager()
//let httpHelper = HTTPHelper()

struct waitlist{
    var name = "Un-Named"
    var size = "0"
    var phone = "No Phone"
}

class WaitListManager: NSObject {
    
    var waitlists = [waitlist]()
    var restaurantName = ""
    
    func addWaitList(name: String, size: String, phone: String){
        waitlists.append(waitlist(name: name, size: size, phone: phone))
    }
    
    func clearWaitList(){
        waitlists.removeAll()
    }
    
    func updateRestaurantName(name: String){
        restaurantName = name
    }
    
    func reload () { //added from selfie
        println("RELOADING WAITLIST")
        // Create HTTP request and set request Body
        let httpRequest = httpHelper.buildRequest("get_waitlist", method: "GET",
            authType: HTTPRequestAuthType.HTTPTokenAuth)
        
        // Send HTTP request to load existing waitlist
        httpHelper.sendRequest(httpRequest, completion: {(data:NSData!, error:NSError!) in
            // Display error
            if error != nil {
                let errorMessage = httpHelper.getErrorMessage(error)
                let errorAlert = UIAlertView(title:"Error", message:errorMessage as String, delegate:nil,
                    cancelButtonTitle:"OK")
                errorAlert.show()
                
                return
            }
            
            var eror: NSError?
            let jsonDataArray = NSJSONSerialization.JSONObjectWithData(data,
                options: NSJSONReadingOptions(0), error: &eror) as! NSArray!
            
            // load the collection view with existing waitlist
            if jsonDataArray != nil {
                self.clearWaitList()
                for partyDataDict in jsonDataArray {
                    
                    var partyname = partyDataDict.valueForKey("name") as! String
                    var partysizeint = partyDataDict.valueForKey("size") as! Int
                    var partysize = "\(partysizeint)"
                    var partyphone = partyDataDict.valueForKey("phone") as! String
                    
                    self.addWaitList(partyname, size: partysize, phone: partyphone)
                }
                
    //            self.tblWaitLists.reloadData()
            }
        })
    }
    
    
}
