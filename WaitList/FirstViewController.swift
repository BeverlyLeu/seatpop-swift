//
//  FirstViewController.swift
//  WaitList List
//
//  Created by BEVERLY LEU on 11/14/14.
//  Copyright (c) 2014 BEVERLY'S LAB. All rights reserved.
//

import UIKit
import MessageUI

class FirstViewController: UIViewController, UITableViewDelegate, UITableViewDataSource, MFMessageComposeViewControllerDelegate {
    //test
    @IBOutlet var tblWaitLists: UITableView!
    @IBOutlet var restaurantName: UILabel!
    @IBOutlet var logout: UIButton!
    var shouldFetchNewData = true
   // var dataArray = [waitlist]()
    let httpHelper = HTTPHelper()
    
    var phoneNumber = "111";
    var partyName = "";
    
    
    
    
    
    @IBAction func logoutBtnTapped(sender: AnyObject) {
        println("LOGOUT")
        self.logout.selected = !self.logout.selected
        
        if self.logout.selected {
            println("LOGOUT:SELECTED")
            clearLoggedinFlagInUserDefaults()
            clearDataArrayAndReloadCollectionView()
            clearAPITokensFromKeyChain()
            
            // Set flag to display Sign In view
            shouldFetchNewData = true
            self.viewDidAppear(true)

        }
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
    }
    
    
    
    override func viewDidAppear(animated: Bool) { //ADDED FROM SELFIE
        //logoutBtnTapped()
        println("--------FirstViewController:viewDidAppear")
        //
        
        super.viewDidAppear(true)
        // check if user is signed in
        let defaults = NSUserDefaults.standardUserDefaults()
        
        // is user is not signed in display controller to sign in or sign up
        if defaults.objectForKey("userLoggedIn") == nil {
            println("----NOT USERLOGGEDIN")
            let loginController: ViewController = self.storyboard?.instantiateViewControllerWithIdentifier("ViewController") as! ViewController
            self.tabBarController?.presentViewController(loginController, animated: true, completion: nil)
        } else {
            println("----ELSE USERLOGGEDIN")
            self.loadWaitlistData()
            self.loadRestaurantData()
            // check if API token has expired
            let dateFormatter = NSDateFormatter()
            dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSSZ"
            let userTokenExpiryDate : NSString? = KeychainAccess.passwordForAccount("Auth_Token",
                service: "KeyChainService")
            let dateFromString : NSDate? = dateFormatter.dateFromString(userTokenExpiryDate! as String)
            let now = NSDate()
            
            // BEVERLY let comparision = now.compare(dateFromString!)
            
            // check if should fetch new data
            if shouldFetchNewData {
                println("--------FirstViewController:shouldFetchNewData")
                shouldFetchNewData = false
                self.setNavigationItems()
              //  waitlistMgr.reload()
            }
            
            // logout and ask user to sign in again if token is expired
         // BEVERLY  if comparision != NSComparisonResult.OrderedAscending {
           //BEVERLY     self.logoutBtnTapped()
           //BEVERLY }
        }
        
        restaurantName.text = ""
        //println(waitlistMgr.restaurantName)
    }

    
    
    override func didReceiveMemoryWarning() {
        println("--------FirstViewController:didReceiveMemoryWarning")
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
        
    }
    
    
    
    func setNavigationItems() { //added from selfie
        var logOutBtn = UIBarButtonItem(title: "logout", style: UIBarButtonItemStyle.Plain, target: self, action: Selector("logoutBtnTapped"))
        self.navigationItem.leftBarButtonItem = logOutBtn
        
    }
    
    
    // 1. Clears the NSUserDefaults flag
    func clearLoggedinFlagInUserDefaults() {//added from selfie
        let defaults = NSUserDefaults.standardUserDefaults()
        defaults.removeObjectForKey("userLoggedIn")
        defaults.synchronize()
    }
    
    // 2. Removes the data array
    func clearDataArrayAndReloadCollectionView() {//added from selfie
        waitlistMgr.waitlists.removeAll(keepCapacity: true)
       //BEVERLY self.tableView?.reloadData()
    }
    
    // 3. Clears API Auth token from Keychain
    func clearAPITokensFromKeyChain () {//added from selfie
        // clear API Auth Token
        if let userToken = KeychainAccess.passwordForAccount("Auth_Token", service: "KeyChainService") {
            KeychainAccess.deletePasswordForAccount(userToken, account: "Auth_Token", service: "KeyChainService")
        }
        
        // clear API Auth Expiry
        if let userTokenExpiryDate = KeychainAccess.passwordForAccount("Auth_Token_Expiry",
            service: "KeyChainService") {
                KeychainAccess.deletePasswordForAccount(userTokenExpiryDate, account: "Auth_Token_Expiry",
                    service: "KeyChainService")
        }
    }
    
    func logoutBtnTapped() {//added from selfie
        clearLoggedinFlagInUserDefaults()
        clearDataArrayAndReloadCollectionView()
        clearAPITokensFromKeyChain()
        
        // Set flag to display Sign In view
        shouldFetchNewData = true
        self.viewDidAppear(true)
    }

    func loadWaitlistData () { //added from selfie
        println("--------FirstViewController:loadWaitlistData")
        // Create HTTP request and set request Body
        let httpRequest = httpHelper.buildRequest("get_waitlist", method: "GET",
            authType: HTTPRequestAuthType.HTTPTokenAuth)
        
        println(httpRequest)

        // Send HTTP request to load existing waitlist
        httpHelper.sendRequest(httpRequest, completion: {(data:NSData!, error:NSError!) in
            println("--------FirstViewController:loadWaitlistData:httpHelper.sendRequest")
            // Display error
            if error != nil {
                let errorMessage = self.httpHelper.getErrorMessage(error)
                let errorAlert = UIAlertView(title:"Error", message:errorMessage as String, delegate:nil,
                    cancelButtonTitle:"OK")
                errorAlert.show()
                
                return
            }
            
            var eror: NSError?
            let jsonDataArray = NSJSONSerialization.JSONObjectWithData(data,
                options: NSJSONReadingOptions(0), error: &eror) as! NSArray!
            
            // load the collection view with existing waitlist
            if jsonDataArray != nil {
                
                println("--------FirstViewController:loadWaitlistData:if jsonDataArray != nil")
                waitlistMgr.clearWaitList()
                for partyDataDict in jsonDataArray {
                    
                    var partyname = partyDataDict.valueForKey("name") as! String
                    var partysizeint = partyDataDict.valueForKey("size") as! Int
                    var partysize = "\(partysizeint)"
                    var partyphone = partyDataDict.valueForKey("phone") as! String
                    
                    waitlistMgr.addWaitList(partyname, size: partysize, phone: partyphone)
                }
                
               self.tblWaitLists.reloadData()
            }
        })
    }

    
    func loadRestaurantData() { //added from selfie
        println("--------FirstViewController:loadRestaurantData")
        // Create HTTP request and set request Body
        let httpRequest = httpHelper.buildRequest("get_restaurant", method: "GET",
            authType: HTTPRequestAuthType.HTTPTokenAuth)
        
        println(httpRequest)
        
        // Send HTTP request to load existing waitlist
        httpHelper.sendRequest(httpRequest, completion: {(data:NSData!, error:NSError!) in
            println("--------FirstViewController:loadRestaurantData:httpHelper.sendRequest")
            // Display error
            if error != nil {
                let errorMessage = self.httpHelper.getErrorMessage(error)
                let errorAlert = UIAlertView(title:"Error", message:errorMessage as String, delegate:nil,
                    cancelButtonTitle:"OK")
                errorAlert.show()
                
                return
            }
            
            
            
            var eror: NSError?
            let jsonDataArray = NSJSONSerialization.JSONObjectWithData(data,
                options: NSJSONReadingOptions(0), error: &eror) as! NSDictionary!
            
            // load the collection view with existing waitlist
            if jsonDataArray != nil {
                
                jsonDataArray.enumerateKeysAndObjectsUsingBlock({ (dictKey, dictObj, stopBool) -> Void in
                    
                    var myKey = dictKey as! NSString
                    
                    if myKey == "name" {
                        var myObj = dictObj as! String
                        waitlistMgr.updateRestaurantName(myObj)
                        self.restaurantName.text = myObj
                    }

                })
            }
            })
    }
    
    
    //Returning to View
    override func viewWillAppear(animated: Bool) {
        println("--------FirstViewController:viewWillAppear")
        tblWaitLists.reloadData()
    }
    
    //UITableViewDelete
    func tableView(tableView: UITableView, commitEditingStyle editingStyle: UITableViewCellEditingStyle, forRowAtIndexPath indexPath: NSIndexPath){
        
        if(editingStyle == UITableViewCellEditingStyle.Delete){
            waitlistMgr.waitlists.removeAtIndex(indexPath.row)
            tblWaitLists.reloadData()
        }
    }
    
    //UITableViewDataSource
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int{
        //how many rows to return
        return waitlistMgr.waitlists.count
    }
    
    // Row display. Implementers should *always* try to reuse cells by setting each cell's reuseIdentifier and querying for available reusable cells with dequeueReusableCellWithIdentifier:
    // Cell gets various attributes set automatically based on table (separators) and data source (accessory views, editing controls)
    
   func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell{
        let cell: UITableViewCell = UITableViewCell(style: UITableViewCellStyle.Value1 , reuseIdentifier: "test")
        
        cell.textLabel?.text = waitlistMgr.waitlists[indexPath.row].name
        cell.detailTextLabel?.text = waitlistMgr.waitlists[indexPath.row].size
        cell.accessoryType =  UITableViewCellAccessoryType.DisclosureIndicator
        
        return cell
    }
    
    
    //MessageUI
    func launchMessageComposeViewController() {
        if MFMessageComposeViewController.canSendText() {
            let messageVC = MFMessageComposeViewController()
            messageVC.messageComposeDelegate = self
            messageVC.recipients = [phoneNumber]
            messageVC.body = partyName + " Your table is ready!"
            self.presentViewController(messageVC, animated: true, completion: nil)
        }
        else {
            println("User hasn't setup Messages.app")
        }
    }
    

    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
       /* let alert:UIAlertController = UIAlertController()
        alert.title = "Phone Number"
        alert.message = waitlistMgr.waitlists[indexPath.row].phone
        */
        
        
        func handleCancel(alertView: UIAlertAction!)
        {
            println("User click cancel button")
        }
       
        func handleText(alertView: UIAlertAction!)
        {
            launchMessageComposeViewController()
            println("User click text button")
        }
        phoneNumber = waitlistMgr.waitlists[indexPath.row].phone
        partyName = waitlistMgr.waitlists[indexPath.row].name

        var alert = UIAlertController(title: "Phone Number", message: waitlistMgr.waitlists[indexPath.row].phone, preferredStyle: UIAlertControllerStyle.Alert)
        alert.addAction(UIAlertAction(title: "Close", style: UIAlertActionStyle.Cancel, handler:handleCancel))
        alert.addAction(UIAlertAction(title: "Text", style: UIAlertActionStyle.Default, handler:handleText))
        alert.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.Default, handler:{ (ACTION :UIAlertAction!)in
            println("User click Ok button")
        }))
        self.presentViewController(alert, animated: true, completion: nil)
        
    }
        // this function will be called after the user presses the cancel button or sends the text
    func messageComposeViewController(controller: MFMessageComposeViewController!, didFinishWithResult result: MessageComposeResult) {
        self.dismissViewControllerAnimated(true, completion: nil)
    }
    
}

